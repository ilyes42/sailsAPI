module.exports = {

  attributes: {
    summary: {
      type: 'string',
      isNotEmptyString: true,
      required: true
    },
    description: {
      type: 'string',
      isNotEmptyString: true,
      required: true
    },
    screenshots: {
      type: 'string',
      required: true
    },
    logfile: {
      type: 'string',
      required: true
    },
    date: {
      type: 'string',
      unique: true,
      required: true
    },
    priority: {
      type: 'number',
      isIn: [0, 1, 2, 3], // [lowest, low, high, highest]
      required: true
    },
    issueType: {
      type: 'number',
      isIn: [0, 1, 2], // [bug, story, epic]
      required: true
    },
    nameOfReporter: {
      type: 'string',
      required: true
    },
    solved: {
      type: "boolean",
      defaultsTo: false
    }
  }

};
