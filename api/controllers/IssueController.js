const fs = require('fs');
const glob = require('glob');

const FILES_PATH = 'C:\\screenshots\\';

module.exports = {

  // GET /api/issues
  // get all the issue reports found in the database
  getAllIssues: (req, res) => {
    Issue.find().then(result => { // if the find() promise is resolved
      let [message, status] = result.length ? ["These are all the issues in the database", 200] : ["There is no issue in the database", 404];
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => { // if the find() promise is rejected
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  // GET /api/issues/:id
  // get a specific issue report with its id
  getIssue: (req, res) => {
    let id = _.pick(req.params, ["id"]).id;
    Issue.findOne({
      id: id
    }).then(result => {
      let message = `issue with id ${id} ${result ? "found" : "not found"} !`;
      let status = result ? 200 : 404;
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  // GET /api/issues/solved
  // get all issue reports that are marked as solved
  getSolvedIssues: (req, res) => {
    Issue.find({
      solved: true
    }).then(result => {
      let message = result.length ? "These are all the solved issues in the database !" : "No solved issues found";
      let status = result.length ? 200 : 404;
      res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      res.status(status).json({
        error: error
      });
    });
  },

  // GET /api/issues/unsolved
  // get all issue reports that are not marked as solved
  getUnsolvedIssues: (req, res) => {
    Issue.find({
      solved: false
    }).then(result => {
      let [message, status] = result.length ? ["These are all the unsolved issues in the database !", 200] : ["No unsolved issues found", 404];
      res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      res.status(status).json({
        error: error
      });
    });
  },

  // POST /api/issues
  // add a new issue report to the database
  addIssue: (req, res) => {
    let allowedParams = ["summary", "description", "screenshots", "logfile", "date", "priority", "issueType", "nameOfReporter"];
    let data = _.pick(req.body, allowedParams);
    let path = FILES_PATH + data.date.split(' ')[0] + '\\';
    let fileNameFirstPart = `${data.date.split(' ')[1].replace(/:/g, '_')}`;
    // replace the screenshots data with screenshots directory
    let screenshots = data.screenshots;
    data["screenshots"] = path + fileNameFirstPart;

    Issue.create(data).fetch().then(result => {
      let [message, status] = ["Great! A new issue has been added to the database!", 201]
      // when the data is successfully added to the database,
      // save screenshots in the hard drive

      // Seperate screenshots and save them locally in FILES_PATH
      //###################################################################################

      let files = screenshots.replace(/data:image\/\w+;base64,/g, "").split('%');
      // extract the extension of every file
      let extensions = screenshots.split('%').map(file => {
        return file.match(/(?<=image\/)(.*?)(?=;)/)[0];
      });

      // since fs.writefile() can't create subdirectories we check if the directory exists
      if (!fs.existsSync(path)) {
        fs.mkdirSync(path); // if the directory doesn't exist make it exist!
      }

      for (let i = 0; i < files.length; i++) {
        let buf = new Buffer(files[i], 'base64');
        fs.writeFile(`${path}${fileNameFirstPart}_(${i}).${extensions[i]}`, buf, err => {
          if (err) throw err;
        });
      }

      //###################################################################################

      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      console.log(error);
      return res.status(status).json({
        error: error
      });
    });
  },

  // PUT /api/issues/:id/solve
  // mark a specific issue report as solved
  solveIssue: (req, res) => {
    let id = _.pick(req.params, ["id"]).id;
    Issue.update({
      id: id
    }).set({
      solved: true
    }).fetch().then(result => {
      let message = `issue with id ${id} ${result.length ? 'solved' : "not found"}!`;
      let status = result.length ? 200 : 404;
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  // PUT /api/issues/:id/unsolve
  // mark a specific issue report as not solved
  unsolveIssue: (req, res) => {
    let id = _.pick(req.params, ["id"]).id;
    Issue.update({
      id: id
    }).set({
      solved: false
    }).fetch().then(result => {
      let message = `issue with id ${id} ${result.length ? 'unsolved' : "not found"}!`;
      let status = result.length ? 200 : 404;
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  // DELETE /api/issues/:id
  // remove a specific issue report from the database
  deleteIssue: (req, res) => {
    let id = _.pick(req.params, ["id"]).id;
    Issue.destroy({
      id: id
    }).fetch().then(result => {
      let message = `issue with id ${id} ${result.length ? 'deleted' : 'not found'} !`;
      let status = result.length ? 200 : 404;
      if (result.length) { // delete screenshots from local storage
        // all files that start with the same TIME came from the same issue report
        // that's why we used glob to get all files following the pattern TIME*
        glob(`${result[0].screenshots}*`, (err, files) => {
          if (err) throw err;
          for (const file of files) {
            fs.unlink(file, err => {
              if (err) throw err;
            });
          }
        });
      }
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  // DELETE /api/issues/solved
  // remove all solved issue reports from the database
  deleteSolvedIssues: (req, res) => {
    Issue.destroy({
      solved: true
    }).fetch().then(result => {
      let [message, status] = result.length ? ['All solved issues removed !', 200] : ['No solved issues found', 404];
      if (result.length) {
        for (const r of result) {
          glob(`${r.screenshots}*`, (err, files) => {
            if (err) throw err;
            for (const file of files) {
              fs.unlink(file, err => {
                if (err) throw err;
              });
            }
          });
        }
      }
      return res.status(status).json({
        message: message,
        result: result
      });
    }).catch(error => {
      let status = 400;
      return res.status(status).json({
        error: error
      });
    });
  },

  notFound: (req, res) => {
    return res.status(404).json({
      message: 'The requested url does not exist!'
    });
  }

}
