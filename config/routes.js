module.exports.routes = {

  'GET /api/issues': 'IssueController.getAllIssues',
  'GET /api/issues/:id': 'IssueController.getIssue',
  'GET /api/issues/solved': 'IssueController.getSolvedIssues',
  'GET /api/issues/unsolved': 'IssueController.getUnsolvedIssues',

  'POST /api/issues': 'IssueController.addIssue',
  'POST /login': 'AuthController.login',

  'PUT /api/issues/:id/solve': 'IssueController.solveIssue',
  'PUT /api/issues/:id/unsolve': 'IssueController.unsolveIssue',

  'DELETE /api/issues/:id': 'IssueController.deleteIssue',
  'DELETE /api/issues/solved': 'IssueController.deleteSolvedIssues',

  '*': 'IssueController.notFound'

};
