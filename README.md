This repository represents the Back-end of my Issue Reporting web application project.
You can view the Front-end at: https://gitlab.com/ilyes42/intern-angular-app

How to run this app?

- 1 - Clone the repository on your machine: git clone https://gitlab.com/ilyes42/sailsAPI
- 2 - Change your directory: cd "sailsApi"
- 3 - Install all the dependencies: npm install
- 4 - Run: node app.js

Now, the server is listening on http://localhost:1337